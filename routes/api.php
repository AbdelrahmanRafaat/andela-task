<?php

use Illuminate\Http\Request;

Route::get('/accounts/{accountId}/', 'AccountsController@getAccount');

Route::post('/accounts/{accountId}/deposits/', 'TransactionsController@deposit');
Route::post('/accounts/{accountId}/withdraws/', 'TransactionsController@withdraw');
