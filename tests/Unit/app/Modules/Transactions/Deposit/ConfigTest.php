<?php

namespace Tests\Unit\app\Modules\Transactions\Deposit;

use App\Modules\Transactions\Deposit\Config;
use Tests\TestCase;

/**
 * Class ConfigTest
 *
 * @package Tests\Unit\app\Modules\Transactions\Deposit
 */
class ConfigTest extends TestCase
{
    /**
     * @return void
     */
    public function testGetMaximumAmountPerDay(): void
    {
        putenv('DEPOSIT_TRANSACTIONS_MAXIMUM_AMOUNT_PER_DAY=150000');

        $depositConfig = new Config();
        $this->assertEquals($depositConfig->getMaximumAmountPerDay(), 150000);
    }

    /**
     * @return void
     */
    public function testGetMaximumAmountPerTransaction(): void
    {
        putenv('DEPOSIT_TRANSACTIONS_MAXIMUM_AMOUNT_PER_TRANSACTION=40000');

        $depositConfig = new Config();
        $this->assertEquals($depositConfig->getMaximumAmountPerTransaction(), 40000);
    }

    /**
     * @return void
     */
    public function testGetMaximumCountPerDay(): void
    {
        putenv('DEPOSIT_TRANSACTIONS_MAXIMUM_COUNT_PER_DAY=4');

        $depositConfig = new Config();
        $this->assertEquals($depositConfig->getMaximumCountPerDay(), 4);
    }
}