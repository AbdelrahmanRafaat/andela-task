<?php

namespace Tests\Unit\app\Modules\Transactions\Deposit;

use App\Account;
use App\Modules\Transactions\Deposit\Config;
use App\Modules\Transactions\Deposit\Validator;
use App\Modules\Transactions\Validations\Exceptions\AccountExceededMaximumTransactionsAmountPerDay;
use App\Modules\Transactions\Validations\Exceptions\AccountExceededMaximumTransactionsCountPerDay;
use App\Modules\Transactions\Validations\Exceptions\InvalidTransactionAmount;
use App\Modules\Transactions\Validations\Exceptions\TransactionExceededMaximumAmount;
use App\Transaction;
use Mockery;
use Tests\TestCase;

/**
 * Class ValidatorTest
 *
 * @package Tests\Unit\app\Modules\Transactions\Deposit
 */
class ValidatorTest extends TestCase
{

    /**
     * @return \App\Modules\Transactions\Deposit\Config|\Mockery\MockInterface
     */
    private function getConfigMock(): Mockery\MockInterface
    {
        $configMock = Mockery::mock(Config::class);
        $configMock->allows('getMaximumAmountPerDay')->andReturn(50000);
        $configMock->allows('getMaximumAmountPerTransaction')->andReturn(10000);
        $configMock->allows('getMaximumCountPerDay')->andReturn(5);

        return $configMock;
    }

    /**
     * @return \App\Account
     */
    private function getAccount(): Account
    {
        $account               = new Account(['id' => 10, 'balance' => 7000]);
        $deposits              = collect([New Transaction(['amount' => 2000]), New Transaction(['amount' => 5000])]);
        $account->transactions = $deposits;

        return $account;
    }

    /**
     * @throws \App\Modules\Transactions\Validations\Exceptions\AccountExceededMaximumTransactionsAmountPerDay
     * @throws \App\Modules\Transactions\Validations\Exceptions\AccountExceededMaximumTransactionsCountPerDay
     * @throws \App\Modules\Transactions\Validations\Exceptions\InvalidTransactionAmount
     * @throws \App\Modules\Transactions\Validations\Exceptions\TransactionExceededMaximumAmount
     */
    public function testInvalidTransactionAmount()
    {
        $configMock  = $this->getConfigMock();
        $account     = $this->getAccount();
        $transaction = new Transaction();
        $validator   = new Validator($configMock, $transaction, $account);

        $this->expectException(InvalidTransactionAmount::class);
        $validator->validate();
    }

    /**
     * @throws \App\Modules\Transactions\Validations\Exceptions\AccountExceededMaximumTransactionsAmountPerDay
     * @throws \App\Modules\Transactions\Validations\Exceptions\AccountExceededMaximumTransactionsCountPerDay
     * @throws \App\Modules\Transactions\Validations\Exceptions\InvalidTransactionAmount
     * @throws \App\Modules\Transactions\Validations\Exceptions\TransactionExceededMaximumAmount
     */
    public function testTransactionExceededMaximumAmount()
    {
        $configMock  = $this->getConfigMock();
        $account     = $this->getAccount();
        $transaction = new Transaction(['amount' => $configMock->getMaximumAmountPerTransaction() + 1]);
        $validator   = new Validator($configMock, $transaction, $account);

        $this->expectException(TransactionExceededMaximumAmount::class);
        $validator->validate();
    }

    /**
     * @throws \App\Modules\Transactions\Validations\Exceptions\AccountExceededMaximumTransactionsAmountPerDay
     * @throws \App\Modules\Transactions\Validations\Exceptions\AccountExceededMaximumTransactionsCountPerDay
     * @throws \App\Modules\Transactions\Validations\Exceptions\InvalidTransactionAmount
     * @throws \App\Modules\Transactions\Validations\Exceptions\TransactionExceededMaximumAmount
     */
    public function testAccountExceededMaximumTransactionsCountPerDay()
    {
        $configMock = $this->getConfigMock();
        $account    = $this->getAccount();
        $account->transactions->add(new Transaction(['amount' => 500]));
        $account->transactions->add(new Transaction(['amount' => 500]));
        $account->transactions->add(new Transaction(['amount' => 500]));
        $account->balance += 1500;

        $transaction = new Transaction(['amount' => 500]);
        $validator = new Validator($configMock, $transaction, $account);

        $this->expectException(AccountExceededMaximumTransactionsCountPerDay::class);
        $validator->validate();
    }

    /**
     * @throws \App\Modules\Transactions\Validations\Exceptions\AccountExceededMaximumTransactionsAmountPerDay
     * @throws \App\Modules\Transactions\Validations\Exceptions\AccountExceededMaximumTransactionsCountPerDay
     * @throws \App\Modules\Transactions\Validations\Exceptions\InvalidTransactionAmount
     * @throws \App\Modules\Transactions\Validations\Exceptions\TransactionExceededMaximumAmount
     */
    public function testAccountExceededMaximumTransactionsAmountPerDay()
    {
        $configMock = $this->getConfigMock();
        $account    = $this->getAccount();
        $account->transactions->add(new Transaction(['amount' => 30000]));
        $account->transactions->add(new Transaction(['amount' => 13000]));
        $account->balance += 50000;

        $transaction = new Transaction(['amount' => 500]);
        $validator = new Validator($configMock, $transaction, $account);

        $this->expectException(AccountExceededMaximumTransactionsAmountPerDay::class);
        $validator->validate();
    }
}