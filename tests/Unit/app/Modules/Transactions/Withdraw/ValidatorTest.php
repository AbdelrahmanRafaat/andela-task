<?php

namespace Tests\Unit\app\Modules\Transactions\Withdraw;

use App\Account;
use App\Modules\Transactions\Withdraw\Validator;
use App\Modules\Transactions\Validations\Exceptions\InsufficientAccountBalance;
use App\Modules\Transactions\Validations\Exceptions\InvalidTransactionAmount;
use App\Modules\Transactions\Withdraw\Config;
use App\Transaction;
use Mockery;
use Tests\TestCase;

/**
 * Class ValidatorTest
 *
 * @package Tests\Unit\app\Modules\Transactions\Withdraw\
 */
class ValidatorTest extends TestCase
{
    /**
     * @return \App\Modules\Transactions\Withdraw\Config|\Mockery\MockInterface
     */
    private function getConfigMock(): Mockery\MockInterface
    {
        $configMock = Mockery::mock(Config::class);
        $configMock->allows('getMaximumAmountPerDay')->andReturn(50000);
        $configMock->allows('getMaximumAmountPerTransaction')->andReturn(10000);
        $configMock->allows('getMaximumCountPerDay')->andReturn(5);

        return $configMock;
    }

    /**
     * @return \App\Account
     */
    private function getAccount(): Account
    {
        $account               = new Account(['id' => 10, 'balance' => 7000]);
        $deposits              = collect([New Transaction(['amount' => 2000]), New Transaction(['amount' => 5000])]);
        $account->transactions = $deposits;

        return $account;
    }

    /**
     * @throws \App\Modules\Transactions\Validations\Exceptions\AccountExceededMaximumTransactionsAmountPerDay
     * @throws \App\Modules\Transactions\Validations\Exceptions\AccountExceededMaximumTransactionsCountPerDay
     * @throws \App\Modules\Transactions\Validations\Exceptions\InsufficientAccountBalance
     * @throws \App\Modules\Transactions\Validations\Exceptions\InvalidTransactionAmount
     * @throws \App\Modules\Transactions\Validations\Exceptions\TransactionExceededMaximumAmount
     */
    public function testAccountHasSufficientBalance(): void
    {
        $configMock  = $this->getConfigMock();
        $account     = $this->getAccount();
        $transaction = new Transaction(['amount' => $account->balance + 1]);
        $validator   = new Validator($configMock, $transaction, $account);

        $this->expectException(InsufficientAccountBalance::class);
        $validator->validate();
    }
}