<?php

namespace Tests\Unit\app\Modules\Transactions\Withdraw;

use App\Modules\Transactions\Withdraw\Config;
use Tests\TestCase;

/**
 * Class ConfigTest
 *
 * @package Tests\Unit\app\Modules\Transactions\Withdraw
 */
class ConfigTest extends TestCase
{
    /**
     * @return void
     */
    public function testGetMaximumAmountPerDay(): void
    {
        putenv('WITHDRAW_TRANSACTIONS_MAXIMUM_AMOUNT_PER_DAY=50000');

        $depositConfig = new Config();
        $this->assertEquals($depositConfig->getMaximumAmountPerDay(), 50000);
    }

    /**
     * @return void
     */
    public function testGetMaximumAmountPerTransaction(): void
    {
        putenv('WITHDRAW_TRANSACTIONS_MAXIMUM_AMOUNT_PER_TRANSACTION=20000');

        $depositConfig = new Config();
        $this->assertEquals($depositConfig->getMaximumAmountPerTransaction(), 20000);
    }

    /**
     * @return void
     */
    public function testGetMaximumCountPerDay(): void
    {
        putenv('WITHDRAW_TRANSACTIONS_MAXIMUM_COUNT_PER_DAY=3');

        $depositConfig = new Config();
        $this->assertEquals($depositConfig->getMaximumCountPerDay(), 3);
    }
}