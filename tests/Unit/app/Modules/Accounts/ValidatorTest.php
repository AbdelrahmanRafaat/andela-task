<?php

namespace Tests\Unit\app\Modules\Accounts;

use App\Modules\Accounts\Exceptions\AccountNotFoundException;
use Tests\TestCase;
use App\Modules\Accounts\Validator as AccountValidator;

/**
 * Class ValidatorTest
 *
 * @package Tests\Unit\app\Modules\Accounts
 */
class ValidatorTest extends TestCase
{
    /**
     * @return void
     * @throws \App\Modules\Accounts\Exceptions\AccountNotFoundException
     */
    public function testValidate(): void
    {
        $validator = new AccountValidator(null);
        $this->expectException(AccountNotFoundException::class);
        $validator->validate();
    }
}