<?php


namespace Tests\Unit\app\Modules\Accounts;

use App\Account;
use App\Modules\Accounts\AccountRepository;
use App\Modules\Transactions\Constants;
use App\Transaction;
use Illuminate\Support\Facades\DB;
use Mockery;
use Tests\TestCase;

/**
 * Class AccountRepositoryTest
 *
 * @package Tests\Unit\app\Modules\Accounts
 */
class AccountRepositoryTest extends TestCase
{
    /**
     * @return \App\Account|\Mockery\MockInterface
     */
    private function getAccountModelMock(): Mockery\MockInterface
    {
        $mock = Mockery::mock(Account::class);

        $account          = new Account();
        $account->id      = 10;
        $account->balance = 4500;

        $deposit         = New Transaction();
        $deposit->amount = 5000;
        $deposit->type   = Constants::TRANSACTION_TYPE_DEPOSIT;

        $withDraw         = New Transaction();
        $withDraw->amount = 5000;
        $withDraw->type   = Constants::TRANSACTION_TYPE_WITHDRAW;

        $account->transactions = collect($deposit, $withDraw);

        $mock->allows('find')
             ->with('10')
             ->andReturn($account);

        $mock->allows('find')
             ->with('invalid-account')
             ->andReturn(null);

        $mock->allows('with')
             ->with(Mockery::any())
             ->andReturn($mock);

        return $mock;
    }

    /**
     * @return void
     */
    public function testFindAccount(): void
    {
        $mock              = $this->getAccountModelMock();
        $accountRepository = new AccountRepository($mock);

        $this->assertEquals($accountRepository->findAccount('10')->id, 10);
        $this->assertEmpty($accountRepository->findAccount('invalid-account'));
    }

    /**
     * @return void
     */
    public function testFindAccountWithPreviousDayDeposits(): void
    {
        $mock              = $this->getAccountModelMock();
        $accountRepository = new AccountRepository($mock);

        $this->assertEquals($accountRepository->findAccountWithPreviousDayDeposits('10')->id, 10);
        $this->assertEmpty($accountRepository->findAccountWithPreviousDayDeposits('invalid-account'));
    }

    /**
     * @return void
     */
    public function testFindAccountWithPreviousDayWithdraws(): void
    {
        $mock              = $this->getAccountModelMock();
        $accountRepository = new AccountRepository($mock);

        $this->assertEquals($accountRepository->findAccountWithPreviousDayWithdraws('10')->id, 10);
        $this->assertEmpty($accountRepository->findAccountWithPreviousDayWithdraws('invalid-account'));
    }

    /**
     * @return void
     */
    public function testDeposit(): void
    {
        $mock = $this->getAccountModelMock();
        DB::shouldReceive('transaction')
          ->with(Mockery::any())
          ->andReturn();

        $accountRepository = new AccountRepository($mock);
        $account           = new Account();
        $account->id       = 1000;
        $account->balance  = 4500;

        $transaction         = New Transaction();
        $transaction->amount = 500;

        $accountRepository->deposit($account, $transaction);
        $this->assertEquals($account->balance, 5000);
        $this->assertEquals($transaction->account_id, 1000);
        $this->assertEquals($transaction->type, Constants::TRANSACTION_TYPE_DEPOSIT);

        $account->id = 2000;
        $transaction = New Transaction(['amount' => 1500]);
        $accountRepository->deposit($account, $transaction);
        $this->assertEquals($account->balance, 6500);
        $this->assertEquals($transaction->account_id, 2000);
        $this->assertEquals($transaction->type, Constants::TRANSACTION_TYPE_DEPOSIT);


        $account->id = 500;
        $transaction = New Transaction(['amount' => 200]);
        $accountRepository->deposit($account, $transaction);
        $this->assertEquals($transaction->account_id, 500);
        $this->assertEquals($account->balance, 6700);
        $this->assertEquals($transaction->type, Constants::TRANSACTION_TYPE_DEPOSIT);
    }

    /**
     * @return void
     */
    public function testWithdraw(): void
    {
        $mock = $this->getAccountModelMock();
        DB::shouldReceive('transaction')
          ->with(Mockery::any())
          ->andReturn();

        $accountRepository = new AccountRepository($mock);
        $account           = new Account();
        $account->id       = 1000;
        $account->balance  = 4500;

        $transaction         = New Transaction();
        $transaction->amount = 500;

        $accountRepository->withdraw($account, $transaction);
        $this->assertEquals($account->balance, 4000);
        $this->assertEquals($transaction->account_id, 1000);
        $this->assertEquals($transaction->type, Constants::TRANSACTION_TYPE_WITHDRAW);

        $account->id = 2000;
        $transaction = New Transaction(['amount' => 1500]);
        $accountRepository->withdraw($account, $transaction);
        $this->assertEquals($account->balance, 2500);
        $this->assertEquals($transaction->account_id, 2000);
        $this->assertEquals($transaction->type, Constants::TRANSACTION_TYPE_WITHDRAW);


        $account->id = 500;
        $transaction = New Transaction(['amount' => 200]);
        $accountRepository->withdraw($account, $transaction);
        $this->assertEquals($transaction->account_id, 500);
        $this->assertEquals($account->balance, 2300);
        $this->assertEquals($transaction->type, Constants::TRANSACTION_TYPE_WITHDRAW);
    }
}