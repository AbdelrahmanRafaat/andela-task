#Andela-Task

#### Tech-Stack:
- PHP 7.2
- Laravel 5.8
- Docker & Docker-compose
- MYSQL 5.7 
- nginx
---
####Coding Standards:
- PSR-4
- PSR-2
---
####Coding Practices:
- SOLID
- Composition over inheritance
- Coding for interfaces not implementations
- IOC
- Unit-testing
- The domain code is not dependent on the framework (DDD).
---
####prerequisites
- docker
- git
---
####Getting started:
- clone this repo:
```
    $ git clone git@bitbucket.org:AbdelrahmanRafaat/andela-task.git
```

- run the services (app & db & nginx) using docker-compose:
```
    $ make up
```

- install dependencies:
```
    $ make install
```

- run the migrations:
```
    $ make migrate
```

seed some data (accounts & transactions):
```
    $ make seed
```

Run Tests:
```
    $ make test
```

stop the services:
```
    $ make down
```

---
####after you set up the services, update .env file with values you want:

#####Deposits config:
```
    DEPOSIT_TRANSACTIONS_MAXIMUM_AMOUNT_PER_DAY=150000
    DEPOSIT_TRANSACTIONS_MAXIMUM_AMOUNT_PER_TRANSACTION=40000
    DEPOSIT_TRANSACTIONS_MAXIMUM_COUNT_PER_DAY=4
```

#####withdraws config:
```
    WITHDRAW_TRANSACTIONS_MAXIMUM_AMOUNT_PER_DAY=50000
    WITHDRAW_TRANSACTIONS_MAXIMUM_AMOUNT_PER_TRANSACTION=20000
    WITHDRAW_TRANSACTIONS_MAXIMUM_COUNT_PER_DAY=3
```
---

####endpoints:
``0.0.0.0:80`` .. can be changed in docker & ngnix

**error response in any endpoint: for sure the message & status code will change**
```
{
    "error": "account was not found"
}
```

``GET /api/accounts/{{accountId}}``

**success response:**
```
{
    "balance": 10000
}
```

``POST /api/accounts/{{accountId}}/withdraws``
**request**
```
{
    "amount": 500
}
```
**success response:**
```
{
    "balance": 9500
}
```

---
``POST /api/accounts/{{accountId}}/deposits``
**request**
```
{
    "amount": 500
}
```
**success response:**
```
{
    "balance": 10000
}
```

---
####todos:
- setting CI & CD
- Branching strategy (dev & stage & master)