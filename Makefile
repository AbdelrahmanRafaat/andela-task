up:
	 docker-compose up

down:
    docker-compose down

install:
	 docker-compose exec app composer install

migrate:
    docker-compose exec app php artisan migrate

seed:
    docker-compose exec app php artisan seed

test:
    docker-compose exec app ./vendor/bin/phpunit