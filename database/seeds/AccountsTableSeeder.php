<?php

use Illuminate\Database\Seeder;
use App\Account;
use App\Modules\Transactions\Deposit\Config;
use App\Transaction;
use App\Modules\Transactions\Constants;

/**
 * Class AccountsTableSeeder
 */
class AccountsTableSeeder extends Seeder
{
    /**
     * Seeds accounts
     *
     * @return void
     */
    public function run(): void
    {
        $depositConfig            = new Config();
        $maxDepositPerTransaction = $depositConfig->getMaximumAmountPerTransaction();

        for ($i = 0; $i < 100; $i++) {
            $depositAmount = rand(1, $maxDepositPerTransaction);

            $account = Account::create(['balance' => $depositAmount]);
            Transaction::create(['amount' => $depositAmount, 'account_id' => $account->id, 'type' => Constants::TRANSACTION_TYPE_DEPOSIT]);
        }
    }
}