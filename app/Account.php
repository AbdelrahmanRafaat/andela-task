<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class account
 *
 * @package App
 */
class Account extends Model
{
    /** @var string $table table name */
    protected $table    = "accounts";

    /** @var array $fillable fillable attributes */
    protected $fillable = ['balance'];

    /**
     * One to Many Relation
     * An Account has Many transactions
     * A Transaction belongs to an account
     */
    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }
}
