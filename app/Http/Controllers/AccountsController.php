<?php

namespace App\Http\Controllers;

use App\Modules\Accounts\Contracts\AccountRepositoryInterface;
use App\Modules\Accounts\Validator;
use Illuminate\Http\JsonResponse;

/**
 * Class AccountsController
 *
 * @package App\Http\Controllers
 */
class AccountsController extends Controller
{
    protected $accountRepository;

    /**
     * AccountsController constructor.
     *
     * @param \App\Modules\Accounts\Contracts\AccountRepositoryInterface $accountRepository
     */
    public function __construct(AccountRepositoryInterface $accountRepository)
    {
        $this->accountRepository = $accountRepository;
    }

    /**
     * @param string $accountId
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Modules\Accounts\Exceptions\AccountNotFoundException
     */
    public function getAccount(string $accountId): JsonResponse
    {
        $account = $this->accountRepository->findAccount($accountId);
        (new Validator($account))->validate();

        return response()->json(['balance' => $account->balance]);
    }
}
