<?php

namespace App\Http\Controllers;

use App\Modules\Accounts\Contracts\AccountRepositoryInterface;
use App\Modules\Accounts\Validator as AccountValidator;
use App\Modules\Transactions\Deposit\Config as DepositConfig;
use App\Modules\Transactions\Deposit\Validator as DepositValidator;
use App\Modules\Transactions\Withdraw\Validator as WithdrawValidator;
use App\Modules\Transactions\Withdraw\Config as WithdrawConfig;
use App\Transaction;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class TransactionsController
 *
 * @package App\Http\Controllers
 */
class TransactionsController extends Controller
{
    /** @var \App\Modules\Accounts\Contracts\AccountRepositoryInterface */
    protected $accountRepository;

    /**
     * TransactionsController constructor.
     *
     * @param \App\Modules\Accounts\Contracts\AccountRepositoryInterface $accountRepository
     */
    public function __construct(AccountRepositoryInterface $accountRepository)
    {
        $this->accountRepository = $accountRepository;
    }

    /**
     * @param string                   $accountId
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Modules\Accounts\Exceptions\AccountNotFoundException
     * @throws \App\Modules\Transactions\Validations\Exceptions\AccountExceededMaximumTransactionsAmountPerDay
     * @throws \App\Modules\Transactions\Validations\Exceptions\AccountExceededMaximumTransactionsCountPerDay
     * @throws \App\Modules\Transactions\Validations\Exceptions\InvalidTransactionAmount
     * @throws \App\Modules\Transactions\Validations\Exceptions\TransactionExceededMaximumAmount
     */
    public function deposit(string $accountId, Request $request): JsonResponse
    {
        $account = $this->accountRepository->findAccountWithPreviousDayDeposits($accountId);
        (new AccountValidator($account))->validate();

        $transaction      = new Transaction(['amount' => $request->amount ?? 0]);
        $depositConfig    = new DepositConfig();
        $depositValidator = new DepositValidator($depositConfig, $transaction, $account);

        $depositValidator->validate();
        $this->accountRepository->deposit($account, $transaction);

        return response()->json(['balance' => $account->balance]);
    }

    /**
     * @param string                   $accountId
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Modules\Accounts\Exceptions\AccountNotFoundException
     * @throws \App\Modules\Transactions\Validations\Exceptions\AccountExceededMaximumTransactionsAmountPerDay
     * @throws \App\Modules\Transactions\Validations\Exceptions\AccountExceededMaximumTransactionsCountPerDay
     * @throws \App\Modules\Transactions\Validations\Exceptions\InsufficientAccountBalance
     * @throws \App\Modules\Transactions\Validations\Exceptions\InvalidTransactionAmount
     * @throws \App\Modules\Transactions\Validations\Exceptions\TransactionExceededMaximumAmount
     */
    public function withdraw(string $accountId, Request $request): JsonResponse
    {
        $account = $this->accountRepository->findAccountWithPreviousDayWithdraws($accountId);
        (new AccountValidator($account))->validate();

        $transaction       = new Transaction(['amount' => $request->amount ?? 0]);
        $withdrawConfig    = new WithdrawConfig();
        $withdrawValidator = new WithdrawValidator($withdrawConfig, $transaction, $account);

        $withdrawValidator->validate();
        $this->accountRepository->withdraw($account, $transaction);

        return response()->json(['balance' => $account->balance]);
    }
}
