<?php

namespace App\Modules\Accounts;

use App\Account;
use App\Modules\Accounts\Contracts\ValidatorInterface;
use App\Modules\Accounts\Exceptions\AccountNotFoundException;

/**
 * Class Validator
 *
 * @package App\Modules\Accounts
 */
class Validator implements ValidatorInterface
{
    /**
     * @var \App\Account|null
     */
    protected $account;

    /**
     * Validator constructor.
     *
     * @param \App\Account|null $account
     */
    public function __construct(?Account $account)
    {
        $this->account = $account;
    }

    /**
     * @throws \App\Modules\Accounts\Exceptions\AccountNotFoundException
     */
    public function validate(): void
    {
        if (empty($this->account)){
            throw new AccountNotFoundException();
        }
    }
}