<?php

namespace App\Modules\Accounts\Exceptions;

use \Exception;
use Illuminate\Http\JsonResponse;

/**
 * Class AccountNotFoundException
 *
 * @package App\Modules\Accounts\Exceptions
 */
class AccountNotFoundException extends Exception
{
    const MESSAGE          = 'account was not found';
    const CODE             = 4000;
    const HTTP_STATUS_CODE = 404;

    /**
     * AccountNotFoundException constructor.
     */
    public function __construct()
    {
        parent::__construct(self::MESSAGE, self::CODE, null);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render(): JsonResponse
    {
        return response()->json(['error' => $this->getMessage()], self::HTTP_STATUS_CODE);
    }
}