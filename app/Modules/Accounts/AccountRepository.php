<?php

namespace App\Modules\Accounts;

use App\Account;
use App\Modules\Accounts\Contracts\AccountRepositoryInterface;
use App\Modules\Transactions\Constants;
use App\Transaction;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

/**
 * Class AccountRepository
 *
 * @package App\Modules\Accounts
 */
class AccountRepository implements AccountRepositoryInterface
{
    /** @var \App\Account */
    protected $model;

    /**
     * AccountRepository constructor.
     *
     * @param \App\Account $model
     */
    public function __construct(Account $model)
    {
        $this->model = $model;
    }

    /**
     * @param string $accountId
     *
     * @return \App\Account
     */
    public function findAccount(string $accountId): ?Account
    {
        return $this->model->find($accountId);
    }

    /**
     * @param string $accountId
     * @param string $type
     *
     * @return \App\Account
     */
    private function findAccountWithPreviousDayTransactions(string $accountId, string $type): ?Account
    {
        $previousDay = Carbon::now()->subDays(1)->toDateTimeString();

        return $this->model->with([
            'transactions' => function ($query) use ($previousDay, $type) {
                $query->where('created_at', '>=', $previousDay)->where('type', $type);
            },
        ])->find($accountId);
    }

    /**
     * @param string $accountId
     *
     * @return \App\Account|null
     */
    public function findAccountWithPreviousDayDeposits(string $accountId): ?Account
    {
        return $this->findAccountWithPreviousDayTransactions($accountId, Constants::TRANSACTION_TYPE_DEPOSIT);
    }

    /**
     * @param string $accountId
     *
     * @return \App\Account|null
     */
    public function findAccountWithPreviousDayWithdraws(string $accountId): ?Account
    {
        return $this->findAccountWithPreviousDayTransactions($accountId, Constants::TRANSACTION_TYPE_WITHDRAW);
    }

    /**
     * @param \App\Account     $account
     * @param \App\Transaction $transaction
     *
     * @return \App\Account
     */
    private function saveTransaction(Account $account, Transaction $transaction): Account
    {
        DB::transaction(function () use ($account, $transaction) {
            $account->save();
            $transaction->save();
        });

        return $account;
    }

    /**
     * @param \App\Account     $account
     * @param \App\Transaction $transaction
     *
     * @return \App\Account
     */
    public function deposit(Account $account, Transaction $transaction): Account
    {
        $account->balance        += $transaction->amount;
        $transaction->account_id = $account->id;
        $transaction->type       = Constants::TRANSACTION_TYPE_DEPOSIT;

        return $this->saveTransaction($account, $transaction);
    }

    /**
     * @param \App\Account     $account
     * @param \App\Transaction $transaction
     *
     * @return \App\Account
     */
    public function withdraw(Account $account, Transaction $transaction): Account
    {
        $account->balance        -= $transaction->amount;
        $transaction->account_id = $account->id;
        $transaction->type       = Constants::TRANSACTION_TYPE_WITHDRAW;

        return $this->saveTransaction($account, $transaction);
    }
}