<?php


namespace App\Modules\Accounts\Contracts;

use App\Account;
use App\Transaction;

/**
 * Interface AccountRepositoryInterface
 *
 * @package App\Modules\Accounts\Contracts
 */
interface AccountRepositoryInterface
{
    /**
     * @param string $accountId
     *
     * @return \App\Account|null
     */
    public function findAccount(string $accountId): ?Account;

    /**
     * @param string $accountId
     *
     * @return \App\Account
     */
    public function findAccountWithPreviousDayDeposits(string $accountId): ?Account;

    /**
     * @param string $accountId
     *
     * @return \App\Account
     */
    public function findAccountWithPreviousDayWithdraws(string $accountId): ?Account;

    /**
     * @param \App\Account     $account
     * @param \App\Transaction $transaction
     *
     * @return \App\Account
     */
    public function deposit(Account $account, Transaction $transaction): Account;

    /**
     * @param \App\Account     $account
     * @param \App\Transaction $transaction
     *
     * @return \App\Account
     */
    public function withdraw(Account $account, Transaction $transaction): Account;

}