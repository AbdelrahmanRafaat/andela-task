<?php

namespace App\Modules\Accounts\Contracts;

/**
 * Interface ValidatorInterface
 *
 * @package App\Modules\Accounts\Contracts
 */
interface ValidatorInterface
{
    public function validate(): void;
}