<?php


namespace App\Modules\Transactions\Validations\Exceptions;

use App\Modules\Transactions\Contracts\ConfigInterface;
use \Exception;
use Illuminate\Http\JsonResponse;

/**
 * Class AccountExceededMaximumTransactionsCountPerDay
 *
 * @package App\Modules\Transactions\Transaction\Validations\Exceptions
 */
class AccountExceededMaximumTransactionsCountPerDay extends Exception
{
    const MESSAGE          = "transaction exceeded maximum transactions count per day %d";
    const CODE             = 5002;
    const HTTP_STATUS_CODE = 400;

    /**
     * AccountExceededMaximumTransactionsCountPerDay constructor.
     *
     * @param \App\Modules\Transactions\Contracts\ConfigInterface $transactionConfig
     */
    public function __construct(ConfigInterface $transactionConfig)
    {
        parent::__construct(sprintf(self::MESSAGE, $transactionConfig->getMaximumCountPerDay()), self::CODE, null);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render(): JsonResponse
    {
        return response()->json(['error' => $this->getMessage()], self::HTTP_STATUS_CODE);
    }
}