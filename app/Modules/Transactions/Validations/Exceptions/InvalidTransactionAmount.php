<?php


namespace App\Modules\Transactions\Validations\Exceptions;

use Exception;
use Illuminate\Http\JsonResponse;

/**
 * Class InvalidTransactionAmount
 *
 * @package App\Modules\Transactions\Validations\Exceptions
 */
class InvalidTransactionAmount extends Exception
{
    const MESSAGE          = "transaction must be greater than 0";
    const CODE             = 5004;
    const HTTP_STATUS_CODE = 400;

    /**
     * InvalidTransactionAmount constructor.
     */
    public function __construct()
    {
        parent::__construct(self::MESSAGE, self::CODE, null);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render(): JsonResponse
    {
        return response()->json(['error' => $this->getMessage()], self::HTTP_STATUS_CODE);
    }
}