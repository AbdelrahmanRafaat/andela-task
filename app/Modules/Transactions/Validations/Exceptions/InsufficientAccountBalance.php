<?php

namespace App\Modules\Transactions\Validations\Exceptions;

use App\Account;
use \Exception;
use Illuminate\Http\JsonResponse;

/**
 * Class InsufficientAccountBalance
 *
 * @package App\Modules\Transactions\Validations\Exceptions
 */
class InsufficientAccountBalance extends Exception
{
    const MESSAGE          = "insufficient account balance";
    const CODE             = 5000;
    const HTTP_STATUS_CODE = 400;

    /**
     * InsufficientAccountBalance constructor.
     *
     * @param \App\Account $account
     */
    public function __construct(Account $account)
    {
        parent::__construct(sprintf(self::MESSAGE, $account->balance), self::CODE, null);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render(): JsonResponse
    {
        return response()->json(['error' => $this->getMessage()], self::HTTP_STATUS_CODE);
    }
}