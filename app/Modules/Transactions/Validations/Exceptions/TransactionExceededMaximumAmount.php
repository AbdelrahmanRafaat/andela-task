<?php

namespace App\Modules\Transactions\Validations\Exceptions;

use App\Modules\Transactions\Contracts\ConfigInterface;
use \Exception;
use Illuminate\Http\JsonResponse;

/**
 * Class TransactionExceededMaximumTransactionAmount
 *
 * @package App\Modules\Transactions\Transaction\Validations\Exceptions
 */
class TransactionExceededMaximumAmount extends Exception
{
    const MESSAGE          = "transaction exceeded maximum transaction amount %.2f";
    const CODE             = 5001;
    const HTTP_STATUS_CODE = 400;

    /**
     * TransactionExceededMaximumAmount constructor.
     *
     * @param \App\Modules\Transactions\Contracts\ConfigInterface $transactionConfig
     */
    public function __construct(ConfigInterface $transactionConfig)
    {
        parent::__construct(sprintf(self::MESSAGE, $transactionConfig->getMaximumAmountPerTransaction()), self::CODE, null);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render(): JsonResponse
    {
        return response()->json(['error' => $this->getMessage()], self::HTTP_STATUS_CODE);
    }
}