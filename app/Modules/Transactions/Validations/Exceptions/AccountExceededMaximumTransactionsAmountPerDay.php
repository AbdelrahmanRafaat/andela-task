<?php

namespace App\Modules\Transactions\Validations\Exceptions;

use App\Modules\Transactions\Contracts\ConfigInterface;
use \Exception;
use Illuminate\Http\JsonResponse;

/**
 * Class AccountExceededMaximumTransactionsAmountPerDay
 *
 * @package App\Modules\Transactions\Transaction\Validations\Exceptions
 */
class AccountExceededMaximumTransactionsAmountPerDay extends Exception
{
    const MESSAGE          = "account exceeded maximum transactions amount per day %.2f";
    const CODE             = 5003;
    const HTTP_STATUS_CODE = 400;

    /**
     * TransactionExceededMaximumAmount constructor.
     *
     * @param \App\Modules\Transactions\Contracts\ConfigInterface $transactionConfig
     */
    public function __construct(ConfigInterface $transactionConfig)
    {
        parent::__construct(sprintf(self::MESSAGE, $transactionConfig->getMaximumAmountPerTransaction()), self::CODE, null);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render(): JsonResponse
    {
        return response()->json(['error' => $this->getMessage()], self::HTTP_STATUS_CODE);
    }
}