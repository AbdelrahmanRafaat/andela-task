<?php

namespace App\Modules\Transactions\Validations;

use App\Account;
use App\Modules\Transactions\Contracts\ConfigInterface;
use App\Modules\Transactions\Contracts\ValidatorInterface;
use App\Modules\Transactions\Validations\Exceptions\AccountExceededMaximumTransactionsAmountPerDay;
use App\Modules\Transactions\Validations\Exceptions\AccountExceededMaximumTransactionsCountPerDay;
use App\Modules\Transactions\Validations\Exceptions\InvalidTransactionAmount;
use App\Modules\Transactions\Validations\Exceptions\TransactionExceededMaximumAmount;
use App\Transaction;

/**
 * Class Validator
 *
 * @package App\Modules\Transactions\Validations
 */
class Validator implements ValidatorInterface
{
    /** @var ConfigInterface */
    protected $transactionConfig;

    /** @var Transaction */
    protected $transaction;

    /** @var Account */
    protected $accountWithPreviousDayTransactions;

    /**
     * Validator constructor.
     *
     * @param \App\Modules\Transactions\Contracts\ConfigInterface $transactionConfig
     * @param \App\Transaction                                    $transaction
     * @param \App\Account                                        $accountWithPreviousDayTransactions
     */
    public function __construct(ConfigInterface $transactionConfig, Transaction $transaction, Account $accountWithPreviousDayTransactions)
    {
        $this->transaction                        = $transaction;
        $this->accountWithPreviousDayTransactions = $accountWithPreviousDayTransactions;
        $this->transactionConfig                  = $transactionConfig;
    }

    /**
     * @return void
     *
     * @throws \App\Modules\Transactions\Validations\Exceptions\AccountExceededMaximumTransactionsCountPerDay
     * @throws \App\Modules\Transactions\Validations\Exceptions\AccountExceededMaximumTransactionsAmountPerDay
     * @throws \App\Modules\Transactions\Validations\Exceptions\InvalidTransactionAmount
     * @throws \App\Modules\Transactions\Validations\Exceptions\TransactionExceededMaximumAmount
     */
    public function validate(): void
    {
        if ($this->transaction->amount == 0) {
            throw new InvalidTransactionAmount();
        }

        if ($this->transactionExceededMaximumAmount()) {
            throw new TransactionExceededMaximumAmount($this->transactionConfig);
        }

        if ($this->accountExceededMaximumTransactionsCountPerDay()) {
            throw new AccountExceededMaximumTransactionsCountPerDay($this->transactionConfig);
        }

        if ($this->accountExceededMaximumTransactionsAmountPerDay()) {
            throw new AccountExceededMaximumTransactionsAmountPerDay($this->transactionConfig);
        }
    }

    /**
     * @return bool
     */
    private function accountExceededMaximumTransactionsAmountPerDay(): bool
    {
        $todayTransactions      = $this->accountWithPreviousDayTransactions->transactions;
        $todayTransactionsTotal = 0;
        foreach ($todayTransactions as $transaction) {
            $todayTransactionsTotal += $transaction->amount;
        }

        return $todayTransactionsTotal + $this->transaction->amount > $this->transactionConfig->getMaximumAmountPerDay();
    }

    /**
     * @return bool
     */
    private function accountExceededMaximumTransactionsCountPerDay(): bool
    {
        return $this->accountWithPreviousDayTransactions->transactions->count() + 1 > $this->transactionConfig->getMaximumCountPerDay();
    }

    /**
     * @return bool
     */
    private function transactionExceededMaximumAmount(): bool
    {
        return $this->transaction->amount > $this->transactionConfig->getMaximumAmountPerTransaction();
    }
}