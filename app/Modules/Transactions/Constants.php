<?php

namespace App\Modules\Transactions;

/**
 * Class Constants
 *
 * @package App\Modules\Transactions
 */
class Constants
{
    const TRANSACTION_TYPE_DEPOSIT  = 'deposit';
    const TRANSACTION_TYPE_WITHDRAW = 'withdraw';
}