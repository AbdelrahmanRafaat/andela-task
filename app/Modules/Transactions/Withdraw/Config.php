<?php

namespace App\Modules\Transactions\Withdraw;

use App\Modules\Transactions\Contracts\ConfigInterface;

/**
 * Class Config
 *
 * @package App\Modules\Transactions\Config
 */
class Config implements ConfigInterface
{
    /**
     * @return float
     */
    public function getMaximumAmountPerDay(): float
    {

        return env('WITHDRAW_TRANSACTIONS_MAXIMUM_AMOUNT_PER_DAY', 50000);
    }

    /**
     * @return float
     */
    public function getMaximumAmountPerTransaction(): float
    {
        return env('WITHDRAW_TRANSACTIONS_MAXIMUM_AMOUNT_PER_TRANSACTION', 20000);
    }

    /**
     * @return int
     */
    public function getMaximumCountPerDay(): int
    {
        return env('WITHDRAW_TRANSACTIONS_MAXIMUM_COUNT_PER_DAY', 3);
    }
}