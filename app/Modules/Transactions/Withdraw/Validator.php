<?php

namespace App\Modules\Transactions\Withdraw;

use App\Modules\Transactions\Contracts\ValidatorInterface;
use App\Modules\Transactions\Validations\Exceptions\InsufficientAccountBalance;
use App\Modules\Transactions\Validations\Validator as BaseValidator;

/**
 * Class Validator
 *
 * @package App\Modules\Transactions\Withdraw
 */
class Validator extends BaseValidator implements ValidatorInterface
{
    /**
     * @throws \App\Modules\Transactions\Validations\Exceptions\AccountExceededMaximumTransactionsAmountPerDay
     * @throws \App\Modules\Transactions\Validations\Exceptions\AccountExceededMaximumTransactionsCountPerDay
     * @throws \App\Modules\Transactions\Validations\Exceptions\InsufficientAccountBalance
     * @throws \App\Modules\Transactions\Validations\Exceptions\InvalidTransactionAmount
     * @throws \App\Modules\Transactions\Validations\Exceptions\TransactionExceededMaximumAmount
     */
    public function validate(): void
    {
        parent::validate();

        if (!$this->accountHasSufficientBalance()) {
            throw new InsufficientAccountBalance($this->accountWithPreviousDayTransactions);
        }
    }

    /**
     * @return bool
     */
    public function accountHasSufficientBalance(): bool
    {
        return ($this->accountWithPreviousDayTransactions->balance - $this->transaction->amount) > 0;
    }
}