<?php

namespace App\Modules\Transactions\Deposit;

use App\Modules\Transactions\Validations\Validator as BaseValidator;
use App\Modules\Transactions\Contracts\ValidatorInterface;

/**
 * Class Validator
 *
 * @package App\Modules\Transactions\Transaction
 */
class Validator extends BaseValidator implements ValidatorInterface
{

}