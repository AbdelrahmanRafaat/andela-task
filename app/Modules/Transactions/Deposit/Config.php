<?php

namespace App\Modules\Transactions\Deposit;

use App\Modules\Transactions\Contracts\ConfigInterface;

/**
 * Class Config
 *
 * @package App\Modules\Transactions\Config
 */
class Config implements ConfigInterface
{
    /**
     * @return float
     */
    public function getMaximumAmountPerDay(): float
    {
        return env('DEPOSIT_TRANSACTIONS_MAXIMUM_AMOUNT_PER_DAY', 150000);
    }

    /**
     * @return float
     */
    public function getMaximumAmountPerTransaction(): float
    {
        return env('DEPOSIT_TRANSACTIONS_MAXIMUM_AMOUNT_PER_TRANSACTION', 40000);
    }

    /**
     * @return int
     */
    public function getMaximumCountPerDay(): int
    {
        return env('DEPOSIT_TRANSACTIONS_MAXIMUM_COUNT_PER_DAY', 4);
    }
}