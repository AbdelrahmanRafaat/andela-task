<?php

namespace App\Modules\Transactions\Contracts;

/**
 * Interface ConfigInterface
 *
 * @package App\Modules\Transactions\Config
 */
interface ConfigInterface
{
    /**
     * @return float
     */
    public function getMaximumAmountPerDay(): float;

    /**
     * @return float
     */
    public function getMaximumAmountPerTransaction(): float;

    /**
     * @return int
     */
    public function getMaximumCountPerDay(): int;
}