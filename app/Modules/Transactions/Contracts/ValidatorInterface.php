<?php


namespace App\Modules\Transactions\Contracts;

/**
 * Interface ValidatorInterface
 *
 * @package App\Modules\Transactions\Contracts
 */
interface ValidatorInterface
{
    public function validate(): void;
}