<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Transaction
 *
 * @package App
 */
class Transaction extends Model
{
    /** @var string $table table name */
    protected $table = 'transactions';

    /** @var array $fillable fillable attributes */
    protected $fillable = ['amount', 'type', 'account_id'];

    /**
     * One to Many Relation
     * An Account has Many transactions
     * A Transaction belongs to an account
     */
    public function account()
    {
        return $this->belongsTo(Account::class);
    }
}
