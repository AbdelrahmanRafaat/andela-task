<?php

namespace App\Providers;

use App\Account;
use App\Modules\Accounts\AccountRepository;
use App\Modules\Accounts\Contracts\AccountRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(AccountRepositoryInterface::class, function () {
            return new AccountRepository(new Account());
        });
    }
}
